import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {
  allowNewServer = false;
  serverStatus = 'Server is offLine';
  serverName = 'Server Name';
  serverCreated = false;
  servers = ['Server1', 'Server2'];
  constructor() {
    setTimeout(() => {
      this.allowNewServer = true;
    }, 2000);
  }

  activateServer() {
    this.serverStatus = 'Server is onLine: ' + this.serverName;
    this.serverCreated = true;
    this.servers.push(this.serverName);
  }

  updateServerName(event: Event) {
    this.serverName = (event.target as HTMLInputElement).value;
  }

  ngOnInit() {
  }

}
